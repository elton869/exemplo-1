@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Gerenciamento de Funcionários</div>
                  <div class="panel-body">
                    @if (Session::has('success'))
                      <div class="alert alert-success">{{ Session::get('success') }}</div>
                    @elseif (Session::has('failed'))
                      <div class="alert alert-danger">{{ Session::get('failed') }}</div>
                    @endif
                    <a href="{{ route('employees.create') }}" class="btn btn-success" style="float: right; margin-bottom: 15px;">Novo Funcionário</a>
                    <hr style="clear: both;">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Nome</th>
                          <th>Email</th>
                          <th>Ação</th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach ($employees as $employee)
                            <tr>
                              <th scope="row">{{ $employee->id }}</th>
                              <td>{{ $employee->name }}</td>
                              <td>{{ $employee->email }}</td>
                              <td>
                                <a href="{{ route('employees.show', $employee->id) }}" class="btn btn-info">Visualizar</a>
                                <a href="{{ route('employees.edit', $employee->id) }}" class="btn btn-info">Editar</a>
                                <form style="display: inline-block;" action="{{ route('employees.destroy', $employee->id) }}" method="POST">
                                   {{ csrf_field() }}
                                   <input type="hidden" name="_method" value="DELETE">
                                   <button type="submit" class="btn btn-danger">Excluir</a>
                                </form>
                              </td>
                            </tr>
                          @endforeach
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
